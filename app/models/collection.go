package models

import (
	"github.com/jinzhu/gorm"
	"bitbucket.org/botsolve/tb_deps/l10n"
)

type Collection struct {
	gorm.Model
	Name string
	l10n.LocaleCreatable
}
