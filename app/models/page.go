package models

import (
	"bitbucket.org/botsolve/tb_deps/page_builder"
	"bitbucket.org/botsolve/tb_deps/publish2"
)

type Page struct {
	page_builder.Page

	publish2.Version
	publish2.Schedule
	publish2.Visible
}
