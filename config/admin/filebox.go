package admin

import (
	"bitbucket.org/botsolve/tb_deps/filebox"
	"bitbucket.org/botsolve/tb_deps/roles"

	"bitbucket.org/botsolve/tb_web/config"
	"bitbucket.org/botsolve/tb_web/config/auth"
)

var Filebox *filebox.Filebox

func init() {
	Filebox = filebox.New(config.Root + "/public/downloads")
	Filebox.SetAuth(auth.AdminAuth{})
	dir := Filebox.AccessDir("/")
	dir.SetPermission(roles.Allow(roles.Read, "admin"))
}
