package config

import (
	"html/template"
	"os"

	"github.com/jinzhu/configor"
	"github.com/microcosm-cc/bluemonday"
	_ "bitbucket.org/botsolve/tb_deps/auth/providers/facebook"
	_ "bitbucket.org/botsolve/tb_deps/auth/providers/github"
	_ "bitbucket.org/botsolve/tb_deps/auth/providers/google"
	_ "bitbucket.org/botsolve/tb_deps/auth/providers/twitter"
	"bitbucket.org/botsolve/tb_deps/mailer"
	"bitbucket.org/botsolve/tb_deps/mailer/logger"
	"bitbucket.org/botsolve/tb_deps/redirect_back"
	"bitbucket.org/botsolve/tb_deps/render"
	"bitbucket.org/botsolve/tb_deps/session/manager"
)

type SMTPConfig struct {
	Host     string
	Port     string
	User     string
	Password string
}

var Config = struct {
	Port uint `default:"7000" env:"PORT"`
	DB   struct {
		Name     string `env:"DBName" default:"tobacco"`
		Adapter  string `env:"DBAdapter" default:"mysql"`
		Host     string `env:"DBHost" default:"localhost"`
		Port     string `env:"DBPort" default:"3306"`
		User     string `env:"DBUser"`
		Password string `env:"DBPassword"`
	}
	SMTP     SMTPConfig
	// Github   github.Config
	// Google   google.Config
	// Facebook facebook.Config
	// Twitter  twitter.Config
}{}

var (
	Root         = os.Getenv("GOPATH") + "/src/bitbucket.org/botsolve/tb_web"
	View         *render.Render
	Mailer       *mailer.Mailer
	RedirectBack = redirect_back.New(&redirect_back.Config{
		SessionManager:  manager.SessionManager,
		IgnoredPrefixes: []string{"/auth"},
	})
)

func init() {
	if err := configor.Load(&Config, "config/database.yml", "config/smtp.yml", "config/application.yml"); err != nil {
		panic(err)
	}

	View = render.New(nil)

	htmlSanitizer := bluemonday.UGCPolicy()
	View.RegisterFuncMap("raw", func(str string) template.HTML {
		return template.HTML(htmlSanitizer.Sanitize(str))
	})

	// dialer := gomail.NewDialer(Config.SMTP.Host, Config.SMTP.Port, Config.SMTP.User, Config.SMTP.Password)
	// sender, err := dialer.Dial()

	// Mailer = mailer.New(&mailer.Config{
	// 	Sender: gomailer.New(&gomailer.Config{Sender: sender}),
	// })
	Mailer = mailer.New(&mailer.Config{
		Sender: logger.New(&logger.Config{}),
	})
}
