package seo

import (
	"bitbucket.org/botsolve/tb_deps/l10n"
	"bitbucket.org/botsolve/tb_deps/seo"
)

type MySEOSetting struct {
	seo.QorSEOSetting
	l10n.Locale
}

type SEOGlobalSetting struct {
	SiteName string
}

var SEOCollection *seo.Collection
