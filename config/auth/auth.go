package auth

import (
	"time"

	"bitbucket.org/botsolve/tb_deps/auth"
	"bitbucket.org/botsolve/tb_deps/auth/authority"
	_ "bitbucket.org/botsolve/tb_deps/auth/providers/facebook"
	_ "bitbucket.org/botsolve/tb_deps/auth/providers/github"
	_ "bitbucket.org/botsolve/tb_deps/auth/providers/google"
	_ "bitbucket.org/botsolve/tb_deps/auth/providers/twitter"
	"bitbucket.org/botsolve/tb_deps/auth_themes/clean"
	"bitbucket.org/botsolve/tb_web/app/models"
	"bitbucket.org/botsolve/tb_web/config"
	"bitbucket.org/botsolve/tb_web/db"
)

var (
	// Auth initialize Auth for Authentication
	Auth = clean.New(&auth.Config{
		DB:         db.DB,
		Render:     config.View,
		Mailer:     config.Mailer,
		UserModel:  models.User{},
		Redirector: auth.Redirector{RedirectBack: config.RedirectBack},
	})

	// Authority initialize Authority for Authorization
	Authority = authority.New(&authority.Config{
		Auth: Auth,
	})
)

func init() {
	// Auth.RegisterProvider(github.New(&config.Config.Github))
	// Auth.RegisterProvider(google.New(&config.Config.Google))
	// Auth.RegisterProvider(facebook.New(&config.Config.Facebook))
	// Auth.RegisterProvider(twitter.New(&config.Config.Twitter))

	Authority.Register("logged_in_half_hour", authority.Rule{TimeoutSinceLastLogin: time.Minute * 30})
}
