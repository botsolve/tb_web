package i18n

import (
	"path/filepath"

	"bitbucket.org/botsolve/tb_deps/i18n"
	"bitbucket.org/botsolve/tb_deps/i18n/backends/database"
	"bitbucket.org/botsolve/tb_deps/i18n/backends/yaml"

	"bitbucket.org/botsolve/tb_web/config"
	"bitbucket.org/botsolve/tb_web/db"
)

var I18n *i18n.I18n

func init() {
	I18n = i18n.New(database.New(db.DB), yaml.New(filepath.Join(config.Root, "config/locales")))
}
