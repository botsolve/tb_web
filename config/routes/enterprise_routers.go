// +build enterprise

package routes

import "bitbucket.org/botsolve/tb_web/config/admin"

func init() {
	Router()
	WildcardRouter.AddHandler(admin.MicroSite)
}
