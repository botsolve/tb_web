package main

import (
	"bitbucket.org/botsolve/tb_deps/middlewares"
	"bitbucket.org/botsolve/tb_web/config/admin/bindatafs"
	"bitbucket.org/botsolve/tb_deps/session"

	"flag"
	"fmt"
	"html/template"
	"net/http"
	"os"

	"bitbucket.org/botsolve/tb_deps/action_bar"
	"bitbucket.org/botsolve/tb_deps/i18n/inline_edit"
	"bitbucket.org/botsolve/tb_deps/qor"
	"bitbucket.org/botsolve/tb_web/app/models"
	"bitbucket.org/botsolve/tb_web/config"
	"bitbucket.org/botsolve/tb_web/config/admin"
	"bitbucket.org/botsolve/tb_web/config/api"
	"bitbucket.org/botsolve/tb_web/config/i18n"
	"bitbucket.org/botsolve/tb_web/config/routes"
	"bitbucket.org/botsolve/tb_web/config/seo"
	"bitbucket.org/botsolve/tb_web/config/utils"
	_ "bitbucket.org/botsolve/tb_web/db/migrations"
	"bitbucket.org/botsolve/tb_deps/render"
	"bitbucket.org/botsolve/tb_deps/session/manager"
	"bitbucket.org/botsolve/tb_deps/widget"
)

func main() {
	cmdLine := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	compileTemplate := cmdLine.Bool("compile-templates", false, "Compile Templates")
	cmdLine.Parse(os.Args[1:])

	mux := http.NewServeMux()
	mux.Handle("/", routes.Router())
	admin.Admin.MountTo("/admin", mux)
	admin.Filebox.MountTo("/downloads", mux)
	api.API.MountTo("/api", mux)

	config.View.FuncMapMaker = func(render *render.Render, req *http.Request, w http.ResponseWriter) template.FuncMap {
		funcMap := template.FuncMap{}

		// Add `t` method
		for key, fc := range inline_edit.FuncMap(i18n.I18n, utils.GetCurrentLocale(req), utils.GetEditMode(w, req)) {
			funcMap[key] = fc
		}

		for key, value := range admin.ActionBar.FuncMap(w, req) {
			funcMap[key] = value
		}

		widgetContext := admin.Widgets.NewContext(&widget.Context{
			DB:         utils.GetDB(req),
			Options:    map[string]interface{}{"Request": req},
			InlineEdit: utils.GetEditMode(w, req),
		})
		for key, fc := range widgetContext.FuncMap() {
			funcMap[key] = fc
		}

		funcMap["flashes"] = func() []session.Message {
			return manager.SessionManager.Flashes(w, req)
		}

		// Add `action_bar` method
		funcMap["render_action_bar"] = func() template.HTML {
			return admin.ActionBar.Actions(action_bar.Action{Name: "Edit SEO", Link: seo.SEOCollection.SEOSettingURL("/help")}).Render(w, req)
		}

		funcMap["render_seo_tag"] = func() template.HTML {
			return seo.SEOCollection.Render(&qor.Context{DB: utils.GetDB(req)}, "Default Page")
		}

		funcMap["get_categories"] = func() (categories []models.Category) {
			utils.GetDB(req).Find(&categories)
			return
		}

		funcMap["current_locale"] = func() string {
			return utils.GetCurrentLocale(req)
		}

		funcMap["current_user"] = func() *models.User {
			return utils.GetCurrentUser(req)
		}

		funcMap["related_products"] = func(cv models.ColorVariation) []models.Product {
			var products []models.Product
			utils.GetDB(req).Preload("ColorVariations").Limit(4).Find(&products, "id <> ?", cv.ProductID)
			return products
		}

		funcMap["other_also_bought"] = func(cv models.ColorVariation) []models.Product {
			var products []models.Product
			utils.GetDB(req).Preload("ColorVariations").Order("id ASC").Limit(8).Find(&products, "id <> ?", cv.ProductID)
			return products
		}

		return funcMap
	}

	if *compileTemplate {
		bindatafs.AssetFS.Compile()
	} else {
		fmt.Printf("Listening on: %v\n", config.Config.Port)
		if err := http.ListenAndServe(fmt.Sprintf(":%d", config.Config.Port), middlewares.Apply(mux)); err != nil {
			panic(err)
		}
	}
}
