package migrations

import (
	"bitbucket.org/botsolve/tb_deps/activity"
	"bitbucket.org/botsolve/tb_deps/auth/auth_identity"
	"bitbucket.org/botsolve/tb_deps/banner_editor"
	"bitbucket.org/botsolve/tb_deps/help"
	"bitbucket.org/botsolve/tb_deps/media/asset_manager"
	"bitbucket.org/botsolve/tb_web/app/models"
	"bitbucket.org/botsolve/tb_web/config/admin"
	"bitbucket.org/botsolve/tb_web/config/seo"
	"bitbucket.org/botsolve/tb_web/db"
	"bitbucket.org/botsolve/tb_deps/transition"
)

func init() {
	AutoMigrate(&asset_manager.AssetManager{})

	AutoMigrate(&models.Product{}, &models.ProductVariation{}, &models.ProductImage{}, &models.ColorVariation{}, &models.ColorVariationImage{}, &models.SizeVariation{})
	AutoMigrate(&models.Color{}, &models.Size{}, &models.Material{}, &models.Category{}, &models.Collection{})

	AutoMigrate(&models.Address{})

	AutoMigrate(&models.Order{}, &models.OrderItem{})

	AutoMigrate(&models.Store{})

	AutoMigrate(&models.Setting{})

	AutoMigrate(&models.User{})

	AutoMigrate(&transition.StateChangeLog{})

	AutoMigrate(&activity.QorActivity{})

	AutoMigrate(&admin.QorWidgetSetting{})

	AutoMigrate(&models.Page{})

	AutoMigrate(&seo.MySEOSetting{})

	AutoMigrate(&models.MediaLibrary{})

	AutoMigrate(&models.Article{})

	AutoMigrate(&help.QorHelpEntry{})

	AutoMigrate(&auth_identity.AuthIdentity{})

	AutoMigrate(&banner_editor.QorBannerEditorSetting{})
}

func AutoMigrate(values ...interface{}) {
	for _, value := range values {
		db.DB.AutoMigrate(value)
	}
}
