// +build enterprise

package migrations

import (
	"bitbucket.org/botsolve/tb_web/config/admin"
)

func init() {
	AutoMigrate(&admin.QorMicroSite{})
}
